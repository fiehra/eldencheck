project setup:

// yarn

development:

1. git clone https://gitlab.com/fiehra/eldencheck.git or clone with ssh

2. yarn install

3. add dev.env in src directory

// dev.env template:

PORT=XXXX <br>
MONGO=mongo-connection <br>

4. yarn server

test:

1. add test.env
2. yarn test
