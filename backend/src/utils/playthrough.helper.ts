import bossRepository from '../boss/boss.repository';
import Boss from '../models/boss.model';
import constants from './constants';

class PlaythroughHelper {
  constructor() {}

  createBossListForPlaythrough(playthroughId: string) {
    constants.allBosses.forEach((boss) => {
      const newBoss = new Boss({
        playthroughId: playthroughId,
        name: boss.name,
        location: boss.location,
        alive: true,
      });
      bossRepository.save(newBoss);
    });
  }
}

export default new PlaythroughHelper();
