class Constants {
  allBosses = [
    {
      name: 'beastman of farum azula',
      location: 'limgrave',
    },
    {
      name: 'bell bearing hunter',
      location: 'limgrave',
    },
    {
      name: 'black knife assassin',
      location: 'limgrave',
    },
    {
      name: 'bloodhound knight darriwil',
      location: 'limgrave',
    },
    {
      name: 'crucible knight',
      location: 'limgrave',
    },
    {
      name: 'deathbird (weeping)',
      location: 'limgrave',
    },
    {
      name: 'demi-human chief',
      location: 'limgrave',
    },
    {
      name: 'erdtree burial watchdog (stormfoot catacombs)',
      location: 'limgrave',
    },
    {
      name: 'flying dragon agheel',
      location: 'limgrave',
    },
    {
      name: 'godrick the grafted',
      location: 'limgrave',
    },
    {
      name: 'grafted scion',
      location: 'limgrave',
    },
    {
      name: 'grave warden duelist',
      location: 'limgrave',
    },
    {
      name: 'guardian golem',
      location: 'limgrave',
    },
    {
      name: 'mad pumpkin head',
      location: 'limgrave',
    },
    {
      name: 'margit, the fell omen',
      location: 'limgrave',
    },
    {
      name: "night's cavalry",
      location: 'limgrave',
    },
    {
      name: 'patches',
      location: 'limgrave',
    },
    {
      name: 'soldier of godrick',
      location: 'limgrave',
    },
    {
      name: 'stonedigger troll',
      location: 'limgrave',
    },
    {
      name: 'tibia mariner',
      location: 'limgrave',
    },
    {
      name: 'tree sentinel',
      location: 'limgrave',
    },
    {
      name: 'ulcerated tree spirit',
      location: 'limgrave',
    },
    {
      name: 'ancient hero of zamor',
      location: 'limgrave',
    },
    {
      name: 'cemetery shade',
      location: 'limgrave',
    },
    {
      name: 'deathbird',
      location: 'limgrave',
    },
    {
      name: 'erdtree avatar',
      location: 'limgrave',
    },
    {
      name: "erdtree burial watchdog (impaler's catacombs)",
      location: 'limgrave',
    },
    {
      name: 'leonine misbegotten',
      location: 'limgrave',
    },
    {
      name: 'miranda the blighted bloom',
      location: 'limgrave',
    },
    {
      name: "night's cavalry (weeping)",
      location: 'limgrave',
    },
    {
      name: 'runebear',
      location: 'limgrave',
    },
    {
      name: 'scaly misbegotten',
      location: 'limgrave',
    },
    {
      name: 'adan, thief of fire',
      location: 'liurnia',
    },
    {
      name: 'alecto, black knife ringleader',
      location: 'liurnia',
    },
    {
      name: 'bell bearing hunter',
      location: 'liurnia',
    },
    {
      name: 'black knife assassin',
      location: 'liurnia',
    },
    {
      name: 'bloodhound knight',
      location: 'liurnia',
    },
    {
      name: 'bols, carian knight',
      location: 'liurnia',
    },
    {
      name: 'cemetery shade',
      location: 'liurnia',
    },
    {
      name: 'cleanrot knight',
      location: 'liurnia',
    },
    {
      name: 'crystalian',
      location: 'liurnia',
    },
    {
      name: 'crystalian spear & crystalian staff (duo)',
      location: 'liurnia',
    },
    {
      name: 'death rite bird',
      location: 'liurnia',
    },
    {
      name: 'deathbird',
      location: 'liurnia',
    },
    {
      name: 'erdtree avatar (liurnia northeast)',
      location: 'liurnia',
    },
    {
      name: 'erdtree avatar (liurnia southwest)',
      location: 'liurnia',
    },
    {
      name: 'erdtree burial watchdog',
      location: 'liurnia',
    },
    {
      name: 'glintstone dragon adula',
      location: 'liurnia',
    },
    {
      name: 'glintstone dragon smarag',
      location: 'liurnia',
    },
    {
      name: 'magma wyrm makar',
      location: 'liurnia',
    },
    {
      name: "night's cavalry (liurnia north)",
      location: 'liurnia',
    },
    {
      name: "night's cavalry (liurnia south)",
      location: 'liurnia',
    },
    {
      name: 'omenkiller',
      location: 'liurnia',
    },
    {
      name: 'onyx lord',
      location: 'liurnia',
    },
    {
      name: 'red wolf of radagon',
      location: 'liurnia',
    },
    {
      name: 'rennala, queen of the full moon',
      location: 'liurnia',
    },
    {
      name: 'royal knight loretta',
      location: 'liurnia',
    },
    {
      name: 'royal revenant',
      location: 'liurnia',
    },
    {
      name: 'spirit-caller snail',
      location: 'liurnia',
    },
    {
      name: 'tibia mariner',
      location: 'liurnia',
    },
    {
      name: 'cemetery shade',
      location: 'caelid',
    },
    {
      name: "commander o'neil",
      location: 'caelid',
    },
    {
      name: 'crucible knight & misbegotten warrior',
      location: 'caelid',
    },
    {
      name: 'death rite bird',
      location: 'caelid',
    },
    {
      name: 'decaying ekzykes',
      location: 'caelid',
    },
    {
      name: 'erdtree avatar',
      location: 'caelid',
    },
    {
      name: 'erdtree burial watchdog (duo)',
      location: 'caelid',
    },
    {
      name: 'fallingstar beast',
      location: 'caelid',
    },
    {
      name: 'frenzied duelist',
      location: 'caelid',
    },
    {
      name: 'mad pumpkin heads',
      location: 'caelid',
    },
    {
      name: 'magma wyrm',
      location: 'caelid',
    },
    {
      name: "night's cavalry (dragonbarrow)",
      location: 'caelid',
    },
    {
      name: 'nox swordstress & nox priest',
      location: 'caelid',
    },
    {
      name: 'starscourge radahn',
      location: 'caelid',
    },
    {
      name: 'battlemage hugues',
      location: 'caelid',
    },
    {
      name: 'beastman of farum azula (duo)',
      location: 'caelid',
    },
    {
      name: 'bell bearing hunter',
      location: 'caelid',
    },
    {
      name: 'black blade kindred',
      location: 'caelid',
    },
    {
      name: 'cleanrot knight (duo)',
      location: 'caelid',
    },
    {
      name: 'flying dragon greyll',
      location: 'caelid',
    },
    {
      name: 'godskin apostle',
      location: 'caelid',
    },
    {
      name: "night's cavalry (caelid)",
      location: 'caelid',
    },
    {
      name: 'putrid avatar',
      location: 'caelid',
    },
    {
      name: 'putrid crystalian trio',
      location: 'caelid',
    },
    {
      name: 'putrid tree spirit',
      location: 'caelid',
    },
    {
      name: 'ancient dragon lansseax',
      location: 'altus',
    },
    {
      name: 'ancient hero of zamor',
      location: 'altus',
    },
    {
      name: "black knife assassin (sage's cave)",
      location: 'altus',
    },
    {
      name: "black knife assassin (sainted hero's grave)",
      location: 'altus',
    },
    {
      name: 'crystalian spear & crystalian ringblade',
      location: 'altus',
    },
    {
      name: 'demi-human queen gilika',
      location: 'altus',
    },
    {
      name: 'elemer of the briar',
      location: 'altus',
    },
    {
      name: 'erdtree burial watchdog',
      location: 'altus',
    },
    {
      name: 'fallingstar beast',
      location: 'altus',
    },
    {
      name: 'godefroy the grafted',
      location: 'altus',
    },
    {
      name: 'godskin apostle',
      location: 'altus',
    },
    {
      name: 'necromancer garris',
      location: 'altus',
    },
    {
      name: "night's cavalry (limgrave)",
      location: 'altus',
    },
    {
      name: 'omenkiller & miranda the blighted bloom',
      location: 'altus',
    },
    {
      name: 'perfumer tricia & misbegotten warrior',
      location: 'altus',
    },
    {
      name: 'sanguine noble',
      location: 'altus',
    },
    {
      name: 'stonedigger troll',
      location: 'altus',
    },
    {
      name: 'tibia mariner',
      location: 'altus',
    },
    {
      name: 'tree sentinel (duo)',
      location: 'altus',
    },
    {
      name: 'wormface',
      location: 'altus',
    },
    {
      name: 'abductor virgins (duo)',
      location: 'mt. gelmir',
    },
    {
      name: 'demi-human queen maggie',
      location: 'mt. gelmir',
    },
    {
      name: 'demi-human queen margot',
      location: 'mt. gelmir',
    },
    {
      name: 'full-grown fallingstar beast',
      location: 'mt. gelmir',
    },
    {
      name: 'god-devouring serpent / rykard, lord of blasphemy',
      location: 'mt. gelmir',
    },
    {
      name: 'godskin noble',
      location: 'mt. gelmir',
    },
    {
      name: 'kindred of rot (duo)',
      location: 'mt. gelmir',
    },
    {
      name: 'magma wyrm',
      location: 'mt. gelmir',
    },
    {
      name: 'red wolf of the champion',
      location: 'mt. gelmir',
    },
    {
      name: 'ulcerated tree spirit',
      location: 'mt. gelmir',
    },
    {
      name: 'bell bearing hunter',
      location: 'altus',
    },
    {
      name: 'crucible knight & crucible knight ordovis',
      location: 'altus',
    },
    {
      name: 'deathbird',
      location: 'altus',
    },
    {
      name: 'draconic tree sentinel',
      location: 'altus',
    },
    {
      name: 'fell twins',
      location: 'altus',
    },
    {
      name: 'grave warden duelist',
      location: 'altus',
    },
    {
      name: 'onyx lord',
      location: 'altus',
    },
    {
      name: 'esgar, priest of blood',
      location: 'leyndell',
    },
    {
      name: 'godfrey, first elden lord',
      location: 'leyndell',
    },
    {
      name: 'mohg, the omen',
      location: 'leyndell',
    },
    {
      name: 'morgott, the omen king',
      location: 'leyndell',
    },
    {
      name: 'ancient hero of zamor',
      location: 'mountaintops',
    },
    {
      name: 'borealis the freezing fog',
      location: 'mountaintops',
    },
    {
      name: 'commander niall',
      location: 'mountaintops',
    },
    {
      name: 'death rite bird',
      location: 'mountaintops',
    },
    {
      name: 'erdtree avatar',
      location: 'mountaintops',
    },
    {
      name: 'fire giant',
      location: 'mountaintops',
    },
    {
      name: 'godskin apostle and godskin noble (spiritcaller snail)',
      location: 'mountaintops',
    },
    {
      name: 'tibia mariner',
      location: 'mountaintops',
    },
    {
      name: 'ulcerated tree spirit',
      location: 'mountaintops',
    },
    {
      name: 'vyke, knight of the roundtable',
      location: 'mountaintops',
    },
    {
      name: 'maliketh, the black blade',
      location: 'farum azula',
    },
    {
      name: 'dragonlord placidusax',
      location: 'farum azula',
    },
    {
      name: 'godskin duo',
      location: 'farum azula',
    },
    {
      name: 'black blade kindred',
      location: 'mountaintops',
    },
    {
      name: "night's cavalry",
      location: 'mountaintops',
    },
    {
      name: 'stray mimic tear',
      location: 'mountaintops',
    },
    {
      name: 'astel, stars of darkness',
      location: 'mountaintops',
    },
    {
      name: 'death rite bird',
      location: 'mountaintops',
    },
    {
      name: 'great wyrm theodorix',
      location: 'mountaintops',
    },
    {
      name: 'misbegotten crusader',
      location: 'mountaintops',
    },
    {
      name: "night's cavalry duo",
      location: 'mountaintops',
    },
    {
      name: 'putrid avatar',
      location: 'mountaintops',
    },
    {
      name: 'putrid grave warden duelist',
      location: 'mountaintops',
    },
    {
      name: 'loretta, knight of the haligtree',
      location: 'haligtree',
    },
    {
      name: 'malenia, blade of miquella',
      location: 'haligtree',
    },
    {
      name: 'ancestor spirit',
      location: 'siofra',
    },
    {
      name: 'dragonkin soldier',
      location: 'siofra',
    },
    {
      name: 'mohg, lord of blood',
      location: 'siofra',
    },
    {
      name: 'dragonkin soldier of nokstella',
      location: 'ainsel',
    },
    {
      name: 'mimic tear',
      location: 'siofra',
    },
    {
      name: 'regal ancestor spirit',
      location: 'siofra',
    },
    {
      name: 'valiant gargoyle & valiant gargoyle (twinblade)',
      location: 'siofra',
    },
    {
      name: 'crucible knight siluria',
      location: 'deeproot',
    },
    {
      name: "fia's champions",
      location: 'deeproot',
    },
    {
      name: 'lichdragon fortissax',
      location: 'deeproot',
    },
    {
      name: 'astel, naturalborn of the void',
      location: 'ainsel',
    },
    {
      name: 'dragonkin soldier',
      location: 'ainsel',
    },
    {
      name: 'godfrey, hoarah loux',
      location: 'ashen capital',
    },
    {
      name: 'sir gideon ofnir, the all-knowing',
      location: 'ashen capital',
    },
    {
      name: 'radagon, elden beast',
      location: 'ashen capital',
    },
  ];
}
export default new Constants();
