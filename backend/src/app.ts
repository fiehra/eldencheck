import * as dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import playthroughRoutes from './playthrough/playthrough.routes';
import bossRoutes from './boss/boss.routes';

const app = express();

app.use(compression());
app.use(helmet());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  }),
);

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization',
  );
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
  next();
});

app.use('/api', playthroughRoutes);
app.use('/api', bossRoutes);

export default app;
