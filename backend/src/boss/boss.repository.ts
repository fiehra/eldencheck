import Boss, { BossInterface } from '../models/boss.model';

class BossRepository {
  save(boss: BossInterface) {
    return Boss.create(boss);
  }

  findAll() {
    // transform object so __v key is ommited
    return Boss.find().sort({ name: 'desc' }).select('-__v');
  }

  findAllBossesFromPlaythrough(id: string) {
    return Boss.find({ playthroughId: id });
  }

  deleteOne(query: any) {
    return Boss.deleteOne(query);
  }

  deleteBossesForPlaythrough(playthroughId: string) {
    return Boss.deleteMany({ playthroughId: playthroughId });
  }

  updateOne(query: any, boss: BossInterface) {
    return Boss.updateOne(query, boss);
  }

  findOne(query: any) {
    return Boss.findOne(query);
  }
}

export default new BossRepository();
