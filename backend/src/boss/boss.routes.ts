import express from 'express';
import BossController from './boss.controller';

class BossRoutes {
  router = express.Router();
  bossController = BossController;

  constructor() {
    this.configureRoutes();
  }

  configureRoutes() {
    this.router.post('/boss/create', this.bossController.create);
    this.router.get(
      '/boss/fetchForPlaythrough/:id',
      this.bossController.getAllForPlaythrough,
    );
    this.router.put('/boss/updateAliveState/:id', this.bossController.updateAliveState);
  }
}

export default new BossRoutes().router;
