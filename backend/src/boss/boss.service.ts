import Boss from '../models/boss.model';
import bossRepository from './boss.repository';

class BossService {
  constructor() {}

  createBoss(req, res) {
    const boss = new Boss({
      playthroughId: req.body.playthroughId,
      name: req.body.name,
      location: req.body.location,
    });
    bossRepository
      .save(boss)
      .then((result) => {
        return res.status(201).json({
          message: 'boss created',
          bossId: result._id,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          error: error,
          message: 'creating boss failed',
        });
      });
  }

  getAllBossesForPlaythrough(req, res) {
    let fetchedBosses;
    bossRepository
      .findAllBossesFromPlaythrough(req.params.id)
      .then((bosses) => {
        fetchedBosses = bosses;
      })
      .then((count) => {
        return res.status(200).json({
          message: 'bosses fetched',
          bosses: fetchedBosses,
          maxBosses: count,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          error: error,
          message: 'fetching bosses failed',
        });
      });
  }

  updateBossAliveState(req, res) {
    let updatedBoss = new Boss({
      _id: req.body._id,
      playthroughId: req.body.playthroughId,
      name: req.body.name,
      location: req.body.location,
      alive: !req.body.alive,
    });
    bossRepository
      .updateOne({ _id: req.params.id }, updatedBoss)
      .then((result) => {
        if (result.modifiedCount > 0) {
          return res.status(200).json({
            boss: updatedBoss,
            message: 'boss killed',
          });
        }
      })
      .catch((error) => {
        return res.status(500).json({
          error: error,
          message: 'killing boss failed',
        });
      });
  }
}
export default new BossService();
