import BossService from './boss.service';

class BossController {
  constructor() {}

  create(req, res, next) {
    BossService.createBoss(req, res);
  }

  getAllForPlaythrough(req, res, next) {
    BossService.getAllBossesForPlaythrough(req, res);
  }

  updateAliveState(req, res, next) {
    BossService.updateBossAliveState(req, res);
  }
}

export default new BossController();
