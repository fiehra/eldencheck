import { Schema, model } from 'mongoose';
// import uniqueValidator from 'mongoose-unique-validator';

export interface PlaythroughInterface {
  _id: string;
  character: string;
  deaths: number;
}

const playthroughSchema = new Schema<PlaythroughInterface>({
  character: { type: Schema.Types.String, required: true },
  deaths: { type: Schema.Types.Number, required: true, default: 0 },
});

// playthroughSchema.plugin(uniqueValidator);
export default model('Playthrough', playthroughSchema);
