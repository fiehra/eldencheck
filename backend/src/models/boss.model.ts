import { Schema, model } from 'mongoose';
// import uniqueValidator from 'mongoose-unique-validator';

export interface BossInterface {
  _id: string;
  playthroughId: string;
  name: string;
  location: string;
  alive: Boolean;
}

const bossSchema = new Schema<BossInterface>({
  playthroughId: { type: Schema.Types.String, required: true },
  name: { type: Schema.Types.String, required: true },
  location: { type: Schema.Types.String, required: true },
  alive: { type: Boolean, default: true },
});

// bossSchema.plugin(uniqueValidator);
export default model('Boss', bossSchema);
