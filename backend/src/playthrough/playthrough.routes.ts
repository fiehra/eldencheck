import express from 'express';
import PlaythroughController from './playthrough.controller';

class PlaythroughRoutes {
  router = express.Router();
  playthroughController = PlaythroughController;

  constructor() {
    this.configureRoutes();
  }

  configureRoutes() {
    this.router.post('/playthrough/create', this.playthroughController.create);
    this.router.get('/playthrough/getAll', this.playthroughController.getAll);
    this.router.get('/playthrough/getOne/:id', this.playthroughController.getOne);
    // this.router.put('/playthrough/update/:id', this.playthroughController.update);
    this.router.put('/playthrough/youDied/:id', this.playthroughController.updateDeaths);
    this.router.delete('/playthrough/delete/:id', this.playthroughController.delete);
  }
}

export default new PlaythroughRoutes().router;
