import Playthrough, { PlaythroughInterface } from '../models/playthrough.model';

class PlaythroughRepository {
  save(playthrough: PlaythroughInterface) {
    return Playthrough.create(playthrough);
  }

  findAll() {
    // transform object so __v key is ommited
    return Playthrough.find().sort({ name: 'desc' }).select('-__v');
  }

  findAllForUser(userId: string) {
    return Playthrough.find().select({ _id: userId });
  }

  deleteOne(query: any) {
    return Playthrough.deleteOne(query);
  }

  updateDeaths(id: string, deaths: number) {
    return Playthrough.findOneAndUpdate({ _id: id }, { deaths: deaths });
  }

  updateOne(query: any, playthrough: PlaythroughInterface) {
    return Playthrough.updateOne(query, playthrough);
  }

  findOne(query: any) {
    return Playthrough.findOne(query);
  }
}

export default new PlaythroughRepository();
