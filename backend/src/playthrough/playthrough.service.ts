import bossRepository from "../boss/boss.repository";
import playthroughHelper from "../utils/playthrough.helper";
import Playthrough from "../models/playthrough.model";
import playthroughRepository from "./playthrough.repository";

class PlaythroughService {
  constructor() {}

  async createPlaythrough(req, res) {
    // let bossList;
    // await bossRepository.findAllBossesFromPlaythrough().then((bosses) => {
    //   bossList = bosses;
    // });
    const playthrough = new Playthrough({
      character: req.body.character,
      bosses: [],
    });
    playthroughRepository
      .save(playthrough)
      .then((result) => {
        playthroughRepository.findAll().then((playthroughs) => {
          const allPlaythroughs = playthroughs;
          playthroughHelper.createBossListForPlaythrough(result?._id);
          return res.status(201).json({
            message: "playthrough created",
            playthroughs: allPlaythroughs,
          });
        });
      })
      .catch((error) => {
        return res.status(500).json({
          error: error,
          message: "creating playthrough failed",
        });
      });
  }

  getAll(req, res) {
    let fetchedPlaythroughs;
    playthroughRepository
      .findAll()
      .then((playthroughs) => {
        fetchedPlaythroughs = playthroughs;
      })
      .then((count) => {
        return res.status(200).json({
          message: "playthroughs fetched",
          playthroughs: fetchedPlaythroughs,
          maxPlaythroughs: count,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          error: error,
          message: "fetching playthroughs failed",
        });
      });
  }

  getPlaythroughById(req, res) {
    let fetchedPlaythrough;
    playthroughRepository
      .findOne({ _id: req.params.id })
      .then((playthrough) => {
        fetchedPlaythrough = playthrough;
        return res.status(200).json({
          message: "playthrough fetched",
          playthrough: fetchedPlaythrough,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          error: error,
          message: "fetching playthrough failed",
        });
      });
  }

  async updatePlaythroughDeaths(req, res) {
    try {
      let updatedDeathCount = req.body.playthrough.deaths + req.body.death;
      if (updatedDeathCount < 0) {
        updatedDeathCount = 0;
      }
      await playthroughRepository.updateDeaths(
        req.body.playthrough._id,
        updatedDeathCount
      );
      let updatedPlaythrough = await playthroughRepository.findOne({
        _id: req.body.playthrough._id,
      });
      if (updatedPlaythrough) {
        return res.status(200).json({
          message: "playthrough updated",
          playthrough: updatedPlaythrough,
        });
      }
    } catch (error) {
      return res.status(500).json({
        error: error,
        message: "updating playthrough failed",
      });
    }
  }

  // updatePlaythrough(req, res) {
  //   const playthrough = new Playthrough({
  //     _id: req.params._id,
  //     name: req.body.name,
  //     wateringFrequency: req.body.wateringFrequency,
  //     light: req.body.light,
  //     thirsty: req.body.thirsty,
  //     lastWater: req.body.lastWater,
  //   });
  //   playthroughRepository
  //     .updateOne({ _id: req.params.id }, playthrough)
  //     .then((result) => {
  //       if (result.modifiedCount > 0) {
  //         return res.status(200).json({
  //           message: 'playthrough updated',
  //         });
  //       }
  //     })
  //     .catch((error) => {
  //       return res.status(500).json({
  //         error: error,
  //         message: 'updating playthrough failed',
  //       });
  //     });
  // }

  deletePlaythrough(req, res) {
    playthroughRepository
      .deleteOne({ _id: req.params.id })
      .then(() => {
        bossRepository.deleteBossesForPlaythrough(req.params.id).then(() => {
          playthroughRepository.findAll().then((playthroughs) => {
            return res.status(200).json({
              playthroughs: playthroughs,
              message: "playthrough deleted",
            });
          });
        });
      })
      .catch((error) => {
        return res.status(500).json({
          error: error,
          message: "deleting playthrough failed",
        });
      });
  }
}
export default new PlaythroughService();
