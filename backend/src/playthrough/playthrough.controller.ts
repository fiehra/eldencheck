import PlaythroughService from './playthrough.service';

class PlaythroughController {
  constructor() {}

  create(req, res, next) {
    PlaythroughService.createPlaythrough(req, res);
  }

  getAll(req, res, next) {
    PlaythroughService.getAll(req, res);
  }

  getOne(req, res, next) {
    PlaythroughService.getPlaythroughById(req, res);
  }

  updateDeaths(req, res, next) {
    PlaythroughService.updatePlaythroughDeaths(req, res);
  }

  // update(req, res, next) {
  //   PlaythroughService.updatePlaythrough(req, res);
  // }

  delete(req, res, next) {
    PlaythroughService.deletePlaythrough(req, res);
  }
}

export default new PlaythroughController();
