export const environment = {
  production: true,
  authRedirectUrl: 'http://localhost:4200/',
  backendUrl: 'http://localhost:3007',
};
