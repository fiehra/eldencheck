import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BossInterface } from '../interfaces/boss.interface';
import {
  PlaythroughInterface,
  AddPlaythroughDTO,
} from '../interfaces/playthrough.interface';

@Injectable({
  providedIn: 'root',
})
export class PlaythroughService {
  constructor(private http: HttpClient) {}

  loadAllPlaythroughs() {
    return this.http.get<{
      message: string;
      playthroughs: PlaythroughInterface[];
      maxCount: number;
    }>(
      environment.backendUrl + '/playthrough/getAll'
      // { headers: this.setAuthHeaders() },
    );
  }

  addPlaythrough(playthrough: AddPlaythroughDTO) {
    return this.http.post<{
      message: string;
      playthroughs: PlaythroughInterface[];
    }>(environment.backendUrl + '/playthrough/create', playthrough);
  }

  // updatePlaythrough(playthrough: PlaythroughInterface) {
  //   return this.http.put<{
  //     message: string;
  //     playthrough: PlaythroughInterface;
  //   }>(environment.backendUrl + '/playthrough/update/' + playthrough._id, playthrough);
  // }

  updateDeaths(playthrough: PlaythroughInterface, death: number) {
    return this.http.put<{
      message: string;
      playthrough: PlaythroughInterface;
    }>(environment.backendUrl + '/playthrough/youDied/' + playthrough._id, {
      playthrough: playthrough,
      death: death,
    });
  }

  deletePlaythrough(playthrough: PlaythroughInterface) {
    return this.http.delete<{
      message: string;
      playthroughs: PlaythroughInterface[];
    }>(environment.backendUrl + '/playthrough/delete/' + playthrough._id);
  }

  getPlaythroughById(id: string) {
    return this.http.get<{
      message: string;
      playthrough: PlaythroughInterface;
    }>(environment.backendUrl + '/playthrough/getOne/' + id);
  }
}
