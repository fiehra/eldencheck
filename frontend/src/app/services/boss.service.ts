import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BossInterface } from '../interfaces/boss.interface';

@Injectable({
  providedIn: 'root',
})
export class BossService {
  constructor(private http: HttpClient) {}

  fetchBossesForPlaythrough(playthroughId: string) {
    return this.http.get<{ message: string; bosses: BossInterface[]; maxCount: number }>(
      environment.backendUrl + '/boss/fetchForPlaythrough/' + playthroughId,
      // { headers: this.setAuthHeaders() },
    );
  }

  updateBossAliveState(boss: BossInterface) {
    return this.http.put<{
      message: string;
      boss: BossInterface;
    }>(environment.backendUrl + '/boss/updateAliveState/' + boss._id, boss);
  }
}
