import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InfrastructureModule } from './modules/infrastructure/infra.module';
import { provideAnimations } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BossEffects, bossReducer } from './store/boss.store';
import { UtilsModule } from './utils/utils.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { PlaythroughModule } from './modules/playthrough/playthrough.module';
import { PlaythroughEffects, playthroughReducer } from './store/playthrough.store';
import { CoreModule } from './core/core.module';

@NgModule({ declarations: [AppComponent],
    bootstrap: [AppComponent], imports: [BrowserModule,
        AppRoutingModule,
        UtilsModule,
        InfrastructureModule,
        CoreModule,
        PlaythroughModule,
        StoreModule.forRoot({
            bossesState: bossReducer,
            playthroughState: playthroughReducer,
        }),
        EffectsModule.forRoot([BossEffects, PlaythroughEffects])], providers: [provideAnimations(), provideHttpClient(withInterceptorsFromDi())] })
export class AppModule {}
