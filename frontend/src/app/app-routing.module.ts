import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEditPlaythroughComponent } from './modules/playthrough/components/add-edit-playthrough/add-edit-playthrough.component';
import { PlaythroughDetailsComponent } from './modules/playthrough/playthrough-details/playthrough-details.component';
import { PlaythroughListComponent } from './modules/playthrough/playthrough-list/playthrough-list.component';

const routes: Routes = [
  { path: '', component: PlaythroughListComponent },
  { path: 'addPlaythrough', component: AddEditPlaythroughComponent },
  { path: 'playthrough/:id', component: PlaythroughDetailsComponent },
  { path: 'playthroughList', component: PlaythroughListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
