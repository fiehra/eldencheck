class Constants {
  locations = [
    'limgrave',
    'altus',
    'caelid',
    'mt. gelmir',
    'mountaintops',
    'liurnia',
    'farum azula',
    'siofra',
    'leyndell',
    'ainsel',
    'deeproot',
    'ashen capital',
    'haligtree',
  ];
}

export default new Constants();
