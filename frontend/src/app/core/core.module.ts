import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { BossListElementComponent } from '../modules/playthrough/components/boss-list-element/boss-list-element.component';
import { UtilsModule } from '../utils/utils.module';
import { DropdownFilterComponent } from './dropdown-filter/dropdown-filter.component';

@NgModule({
  declarations: [ToolbarComponent, DropdownFilterComponent],
  imports: [RouterModule, UtilsModule],
  exports: [ToolbarComponent, DropdownFilterComponent],
})
export class CoreModule {}
