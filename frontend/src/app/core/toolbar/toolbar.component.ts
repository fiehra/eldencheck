import { Component } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  NavigationEnd,
  Router,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, filter, map } from 'rxjs';
import { PlaythroughInterface } from 'src/app/interfaces/playthrough.interface';
import { AppState } from 'src/app/store/app.reducer';
import {
  selectPlaythrough,
  selectShowFilter,
  toggleFilter,
} from 'src/app/store/playthrough.store';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {
  playthrough$: Observable<PlaythroughInterface> = this.store.select(selectPlaythrough);
  showFilter$: Observable<boolean> = this.store.select(selectShowFilter);
  playthroughDetailsPage: boolean = false;
  activeRoute$: string = this.router.url;
  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.route.snapshot),
        map((route: ActivatedRouteSnapshot) => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
      )
      .subscribe((route: ActivatedRouteSnapshot) => {
        if (route.url[0]?.path === 'playthrough') {
          this.playthroughDetailsPage = true;
        } else {
          this.playthroughDetailsPage = false;
        }
      });
  }

  toggleFilter() {
    this.store.dispatch(toggleFilter());
  }
}
