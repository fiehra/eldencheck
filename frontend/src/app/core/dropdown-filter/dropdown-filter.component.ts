import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import { addRemoveFilter } from 'src/app/store/boss.store';
import Constants from 'src/app/utils/constants';

@Component({
  selector: 'dropdown-filter',
  templateUrl: './dropdown-filter.component.html',
  styleUrls: ['./dropdown-filter.component.scss'],
})
export class DropdownFilterComponent implements OnInit {
  locations: string[] = Constants.locations;
  activeFilters$ = this.store.select((state) => state.bossesState.activeFilters);

  constructor(private store: Store<AppState>) {}

  ngOnInit() {}

  selectOption(option: any) {
    this.store.dispatch(addRemoveFilter({ filter: option }));
  }
}
