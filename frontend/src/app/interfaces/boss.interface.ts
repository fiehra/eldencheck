export interface BossInterface {
  _id: string;
  playthroughId: string;
  name: string;
  location: string;
  alive: Boolean;
}
