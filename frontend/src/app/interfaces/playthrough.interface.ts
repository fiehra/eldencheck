import { BossInterface } from './boss.interface';

export interface PlaythroughInterface {
  _id: string;
  character: string;
  deaths: number;
  bosses: BossInterface[];
}

export interface AddPlaythroughDTO {
  character: string;
}
