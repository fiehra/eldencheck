import {
  createAction,
  props,
  createReducer,
  on,
  createSelector,
} from '@ngrx/store';
import { AppState } from './app.reducer';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { catchError, exhaustMap, map, switchMap } from 'rxjs';
import {
  AddPlaythroughDTO,
  PlaythroughInterface,
} from '../interfaces/playthrough.interface';
import { PlaythroughService } from '../services/playthrough.service';
import { loadBossesForPlaythrough } from './boss.store';

// actions ACTIONS actions
export const addPlaythrough = createAction(
  '[Playthrough] add playthrough',
  props<{ playthrough: AddPlaythroughDTO }>()
);
export const loadPlaythrough = createAction(
  '[Playthrough] load playthrough',
  props<{ id: string }>()
);
export const loadPlaythroughSuccess = createAction(
  '[Playthrough] load playthrough success',
  props<{ playthrough: PlaythroughInterface }>()
);
export const loadPlaythroughs = createAction('[Playthrough] load playthroughs');
export const loadPlaythroughsSuccess = createAction(
  '[Playthrough] load playthroughs success',
  props<{ playthroughs: PlaythroughInterface[] }>()
);
export const deletePlaythrough = createAction(
  '[Playthrough] delete playthrough',
  props<{ playthrough: PlaythroughInterface }>()
);
export const deletePlaythroughSuccess = createAction(
  '[Playthrough] delete playthrough success',
  props<{ playthroughs: PlaythroughInterface[] }>()
);
export const filterBosses = createAction('[Playthrough] filter bosses');
export const filterBossesSuccess = createAction(
  '[Playthrough] filter bosses success',
  props<{ filter: string }>()
);
export const youDied = createAction(
  '[Playthrough] you died',
  props<{ playthrough: PlaythroughInterface; death: number }>()
);
export const youDiedSuccess = createAction(
  '[Playthrough] you died Success',
  props<{ playthrough: PlaythroughInterface }>()
);
export const toggleFilter = createAction('[Playthrough] toggle filter');

// state initialState STATE INITIALSTATE state initialState
export interface PlaythroughState {
  selectedPlaythrough: PlaythroughInterface;
  playthroughs: PlaythroughInterface[];
  showFilter: boolean;
  isLoading: boolean;
}

export const initialState: PlaythroughState = {
  selectedPlaythrough: { _id: '', character: '', deaths: 0, bosses: [] },
  playthroughs: [],
  showFilter: true,
  isLoading: false,
};

// reducer REDUCER reducer
export const playthroughReducer = createReducer(
  initialState,
  on(loadPlaythrough, (state) => ({
    ...state,
    selectedPlaythrough: { _id: '', character: '', deaths: 0, bosses: [] },
    isLoading: true,
  })),
  on(loadPlaythroughSuccess, (state, { playthrough }) => ({
    ...state,
    selectedPlaythrough: playthrough,
    isLoading: false,
  })),
  on(toggleFilter, (state) => ({
    ...state,
    showFilter: !state.showFilter,
  })),
  on(loadPlaythroughs, (state) => ({
    ...state,
    playthroughs: [],
    isLoading: true,
  })),
  on(loadPlaythroughsSuccess, (state, { playthroughs }) => ({
    ...state,
    playthroughs: playthroughs,
    isLoading: false,
  })),
  on(filterBosses, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(filterBossesSuccess, (state, { filter }) => ({
    ...state,
    selectedFilter: filter,
    isLoading: false,
  })),
  on(deletePlaythroughSuccess, (state, { playthroughs }) => ({
    ...state,
    selectedPlaythrough: { _id: '', character: '', deaths: 0, bosses: [] },
    playthroughs: playthroughs,
    isLoading: false,
  })),
  on(youDied, (state, { death }) => ({
    ...state,
    isLoading: true,
  })),
  on(youDiedSuccess, (state, { playthrough }) => ({
    ...state,
    selectedPlaythrough: playthrough,
    isLoading: false,
  }))
);

export const playthroughFeature = (state: AppState) => state.playthroughState;

// selectors SELECTORS selectors
export const selectPlaythrough = createSelector(
  playthroughFeature,
  (state: PlaythroughState) => state.selectedPlaythrough
);
export const selectPlaythroughs = createSelector(
  playthroughFeature,
  (state: PlaythroughState) => state.playthroughs
);
export const selectShowFilter = createSelector(
  playthroughFeature,
  (state: PlaythroughState) => state.showFilter
);

// effects EFFECTS effects
@Injectable()
export class PlaythroughEffects {
  constructor(
    private actions$: Actions,
    private playthroughService: PlaythroughService
  ) {}

  loadPlaythroughs$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Playthrough] load playthroughs'),
      exhaustMap(() =>
        this.playthroughService.loadAllPlaythroughs().pipe(
          switchMap((playthroughs) => {
            return [
              loadPlaythroughsSuccess({
                playthroughs: playthroughs.playthroughs,
              }),
            ];
          }),
          catchError((error) => [])
        )
      )
    )
  );

  loadPlaythrough$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Playthrough] load playthrough'),
      exhaustMap((action: any) =>
        this.playthroughService.getPlaythroughById(action.id).pipe(
          switchMap((response) => {
            return [
              loadPlaythroughSuccess({ playthrough: response.playthrough }),
              loadBossesForPlaythrough({ playthroughId: action.id }),
            ];
          }),
          catchError((error) => [])
        )
      )
    )
  );

  addPlaythrough$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Playthrough] add playthrough'),
      exhaustMap((action: any) =>
        this.playthroughService.addPlaythrough(action.playthrough).pipe(
          switchMap((response) => {
            return [
              loadPlaythroughsSuccess({ playthroughs: response.playthroughs }),
            ];
          }),
          catchError((error) => [])
        )
      )
    )
  );

  deletePlaythrough$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Playthrough] delete playthrough'),
      exhaustMap((action: any) =>
        this.playthroughService.deletePlaythrough(action.playthrough).pipe(
          switchMap((response) => {
            return [
              deletePlaythroughSuccess({ playthroughs: response.playthroughs }),
            ];
          }),
          catchError((error) => [])
        )
      )
    )
  );

  youDied$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Playthrough] you died'),
      exhaustMap((action: any) =>
        this.playthroughService
          .updateDeaths(action.playthrough, action.death)
          .pipe(
            switchMap((response) => {
              return [youDiedSuccess({ playthrough: response.playthrough })];
            }),
            catchError((error) => [])
          )
      )
    )
  );
}
