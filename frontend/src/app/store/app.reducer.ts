import { BossesState } from './boss.store';
import { PlaythroughState } from './playthrough.store';

export interface AppState {
  readonly bossesState: BossesState;
  readonly playthroughState: PlaythroughState;
}
