import { createAction, props, createReducer, on, createSelector } from '@ngrx/store';
import { BossInterface } from 'src/app/interfaces/boss.interface';
import { AppState } from './app.reducer';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { BossService } from '../services/boss.service';
import { catchError, exhaustMap, switchMap } from 'rxjs';

export interface BossesState {
  bosses: BossInterface[];
  isLoading: boolean;
  activeFilters: string[];
}

export const initialState: BossesState = {
  bosses: [],
  isLoading: false,
  activeFilters: [],
};

// actions ACTIONS actions
export const loadBossesForPlaythrough = createAction(
  '[Boss] load bosses for playthrough',
  props<{ playthroughId: string }>(),
);
export const loadBossesForPlaythroughSuccess = createAction(
  '[Boss] load bosses success',
  props<{ bosses: BossInterface[] }>(),
);
export const updateBossAliveState = createAction(
  '[Boss] update boss alive state',
  props<{ boss: BossInterface }>(),
);
export const updateBossAliveStateSuccess = createAction(
  '[Boss] update boss alive state success',
  props<{ updatedBoss: BossInterface }>(),
);
export const addRemoveFilter = createAction(
  '[Boss] add / remove filter',
  props<{ filter: string }>(),
);
export const resetFilter = createAction('[Boss] reset filter');
export const filterAlive = createAction(
  '[Boss] filter alive',
  props<{ filter: string }>(),
);

// reducer REDUCER reducer
export const bossReducer = createReducer(
  initialState,
  on(loadBossesForPlaythrough, (state) => ({
    ...state,
    bosses: [],
    isLoading: true,
  })),
  on(loadBossesForPlaythroughSuccess, (state, { bosses }) => ({
    ...state,
    bosses: bosses,
    isLoading: false,
  })),
  on(updateBossAliveState, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(updateBossAliveStateSuccess, (state, { updatedBoss }) => ({
    ...state,
    bosses: state.bosses
      .map((boss) => ({ ...boss }))
      .map((boss) => {
        if (boss._id === updatedBoss._id) {
          return {
            ...boss,
            alive: updatedBoss.alive,
          };
        } else {
          return boss;
        }
      }),
    isLoading: false,
  })),
  on(addRemoveFilter, (state, { filter }) => {
    // Check if the filter is already in the array
    const filterIndex = state.activeFilters.indexOf(filter);
    if (filterIndex !== -1) {
      // If it is, remove the filter from the array
      return {
        ...state,
        activeFilters: [
          ...state.activeFilters.slice(0, filterIndex),
          ...state.activeFilters.slice(filterIndex + 1),
        ],
      };
    }
    return {
      ...state,
      activeFilters: [...state.activeFilters, filter],
    };
  }),
  on(resetFilter, (state) => ({
    ...state,
    activeFilters: [],
  })),
);

export const bossFeature = (state: AppState) => state.bossesState;

// selectors SELECTORS selectors
export const selectBossesForPlaythrough = createSelector(
  bossFeature,
  (state: BossesState) => state.bosses,
);

export const selectActiveFilters = createSelector(
  bossFeature,
  (state) => state.activeFilters,
);

export const selectDeadBosses = createSelector(bossFeature, (state) =>
  state.bosses.filter((boss) => boss.alive === false),
);

export const selectAliveBosses = createSelector(bossFeature, (state) =>
  state.bosses.filter((boss) => boss.alive === true),
);

export const selectFilteredBosses = createSelector(
  selectBossesForPlaythrough,
  selectActiveFilters,
  (bosses, activeFilters) => {
    if (activeFilters.length === 0) {
      return bosses;
    } else {
      // Filter bosses based on activeFilters
      return bosses.filter((boss) => activeFilters.includes(boss.location));
    }
  },
);

// effects EFFECTS effects effects EFFECTS effects
@Injectable()
export class BossEffects {
  constructor(private actions$: Actions, private bossService: BossService) {}

  loadBossesForPlaythrough$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Boss] load bosses for playthrough'),
      exhaustMap((action: any) =>
        this.bossService.fetchBossesForPlaythrough(action.playthroughId).pipe(
          switchMap((response) => {
            return [loadBossesForPlaythroughSuccess({ bosses: response.bosses })];
          }),
          catchError(() => []),
        ),
      ),
    ),
  );

  updateBossAliveState$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Boss] update boss alive state'),
      exhaustMap((action: any) =>
        this.bossService.updateBossAliveState(action.boss).pipe(
          switchMap((response) => {
            return [
              updateBossAliveStateSuccess({
                updatedBoss: response.boss,
              }),
            ];
          }),
          catchError(() => []),
        ),
      ),
    ),
  );
}
