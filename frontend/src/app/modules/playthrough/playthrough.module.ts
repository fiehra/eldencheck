import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UtilsModule } from 'src/app/utils/utils.module';
import { AddEditPlaythroughComponent } from './components/add-edit-playthrough/add-edit-playthrough.component';
import { PlaythroughListComponent } from './playthrough-list/playthrough-list.component';
import { PlaythroughDetailsComponent } from './playthrough-details/playthrough-details.component';
import { CoreModule } from 'src/app/core/core.module';
import { PlaythroughListElementComponent } from './components/playthrough-list-element/playthrough-list-element.component';
import { BossListElementComponent } from './components/boss-list-element/boss-list-element.component';

@NgModule({
  declarations: [
    AddEditPlaythroughComponent,
    BossListElementComponent,
    PlaythroughListComponent,
    PlaythroughListElementComponent,
    PlaythroughDetailsComponent,
  ],
  imports: [UtilsModule, RouterModule, CoreModule, ReactiveFormsModule],
  exports: [],
})
export class PlaythroughModule {}
