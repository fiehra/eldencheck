import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, combineLatest, take } from 'rxjs';
import { PlaythroughInterface } from 'src/app/interfaces/playthrough.interface';
import { AppState } from 'src/app/store/app.reducer';
import {
  addRemoveFilter,
  filterAlive,
  resetFilter,
  selectFilteredBosses,
} from 'src/app/store/boss.store';
import {
  filterBosses,
  loadPlaythrough,
  selectPlaythrough,
  selectShowFilter,
  youDied,
} from 'src/app/store/playthrough.store';

@Component({
  selector: 'playthrough-details',
  templateUrl: './playthrough-details.component.html',
  styleUrls: ['./playthrough-details.component.scss'],
})
export class PlaythroughDetailsComponent {
  playthrough$: Observable<PlaythroughInterface> = this.store.select(selectPlaythrough);
  bosses$ = this.store.select(selectFilteredBosses);
  activeFilters$ = this.store.select((state) => state.bossesState.activeFilters);
  showFilter$: Observable<boolean> = this.store.select(selectShowFilter);
  isLoading$: Observable<boolean> = this.store.select(
    (state) => state.playthroughState.isLoading,
  );

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      let id = paramMap.get('id') as string;
      this.store.dispatch(loadPlaythrough({ id: id }));
    });
  }

  ngOnInit() {}

  resetFilter() {
    this.store.dispatch(resetFilter());
  }

  editDeaths(death: number) {
    combineLatest([this.isLoading$, this.playthrough$])
      .pipe(take(1))
      .subscribe((res) => {
        if (res[0]) {
          return;
        } else {
          this.store.dispatch(youDied({ playthrough: res[1], death: death }));
        }
      });
  }

  // filterDead() {
  //   this.bosses$ = this.store.select(selectDeadBosses);
  // }

  // filterAlive() {
  //   this.bosses$ = this.store.select(selectAliveBosses);
  // }

  filterAlive(alive: string) {
    this.store.dispatch(addRemoveFilter({ filter: alive }));
  }
}
