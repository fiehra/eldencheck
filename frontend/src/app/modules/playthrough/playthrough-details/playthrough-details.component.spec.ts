import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaythroughDetailsComponent } from './playthrough-details.component';

describe('PlaythroughDetailsComponent', () => {
  let component: PlaythroughDetailsComponent;
  let fixture: ComponentFixture<PlaythroughDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaythroughDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlaythroughDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
