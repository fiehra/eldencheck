import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import { loadPlaythroughs, selectPlaythroughs } from 'src/app/store/playthrough.store';

@Component({
  selector: 'playthrough-list',
  templateUrl: './playthrough-list.component.html',
  styleUrls: ['./playthrough-list.component.scss'],
})
export class PlaythroughListComponent {
  playthroughs$ = this.store.select(selectPlaythroughs);

  constructor(private store: Store<AppState>, private router: Router) {
    this.store.dispatch(loadPlaythroughs());
  }

  ngOnInit() {}
}
