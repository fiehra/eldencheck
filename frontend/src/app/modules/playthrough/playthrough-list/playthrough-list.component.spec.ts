import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaythroughListComponent } from './playthrough-list.component';

describe('PlaythroughListComponent', () => {
  let component: PlaythroughListComponent;
  let fixture: ComponentFixture<PlaythroughListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaythroughListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlaythroughListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
