import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaythroughListElementComponent } from './playthrough-list-element.component';

describe('PlaythroughListElementComponent', () => {
  let component: PlaythroughListElementComponent;
  let fixture: ComponentFixture<PlaythroughListElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaythroughListElementComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlaythroughListElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
