import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { BossInterface } from 'src/app/interfaces/boss.interface';
import { PlaythroughInterface } from 'src/app/interfaces/playthrough.interface';
import { AppState } from 'src/app/store/app.reducer';
import { deletePlaythrough } from 'src/app/store/playthrough.store';

@Component({
  selector: 'playthrough-list-element',
  templateUrl: './playthrough-list-element.component.html',
  styleUrls: ['./playthrough-list-element.component.scss'],
})
export class PlaythroughListElementComponent {
  @Input() boss: BossInterface | null = null;
  @Input() playthrough: PlaythroughInterface | null = null;

  constructor(private store: Store<AppState>, private router: Router) {}

  ngOnInit() {}

  deletePlaythrough(playthrough: PlaythroughInterface) {
    this.store.dispatch(deletePlaythrough({ playthrough }));
  }

  routeToPlaythrough(_id: string) {
    this.router.navigate(['/playthrough/' + _id]);
  }
}
