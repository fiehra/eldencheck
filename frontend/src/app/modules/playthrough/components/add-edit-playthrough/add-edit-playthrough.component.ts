import { Component } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import { addPlaythrough } from 'src/app/store/playthrough.store';

@Component({
  selector: 'add-edit-playthrough',
  templateUrl: './add-edit-playthrough.component.html',
  styleUrls: ['./add-edit-playthrough.component.scss'],
})
export class AddEditPlaythroughComponent {
  playthroughForm: FormGroup = new FormGroup({
    character: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required],
    }),
  });

  constructor(private store: Store<AppState>) {}

  onSubmit() {
    if (this.playthroughForm.valid) {
      this.store.dispatch(addPlaythrough({ playthrough: this.playthroughForm.value }));
      this.playthroughForm.reset();
    } else {
      console.log('invalid form');
    }
  }
}
