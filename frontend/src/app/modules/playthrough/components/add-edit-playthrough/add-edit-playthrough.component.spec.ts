import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPlaythroughComponent } from './add-edit-playthrough.component';

describe('AddEditPlaythroughComponent', () => {
  let component: AddEditPlaythroughComponent;
  let fixture: ComponentFixture<AddEditPlaythroughComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditPlaythroughComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddEditPlaythroughComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
