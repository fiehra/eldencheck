import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { BossInterface } from 'src/app/interfaces/boss.interface';
import { PlaythroughInterface } from 'src/app/interfaces/playthrough.interface';
import { AppState } from 'src/app/store/app.reducer';
import { updateBossAliveState } from 'src/app/store/boss.store';

@Component({
  selector: 'boss-list-element',
  templateUrl: './boss-list-element.component.html',
  styleUrls: ['./boss-list-element.component.scss'],
})
export class BossListElementComponent {
  @Input() boss: BossInterface | null = null;
  @Input() playthrough: PlaythroughInterface | null = null;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {}

  changeState() {
    this.store.dispatch(updateBossAliveState({ boss: this.boss! }));
  }
}
