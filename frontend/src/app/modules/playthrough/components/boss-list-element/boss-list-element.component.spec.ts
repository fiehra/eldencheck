import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BossListElementComponent } from './boss-list-element.component';

describe('BossListElementComponent', () => {
  let component: BossListElementComponent;
  let fixture: ComponentFixture<BossListElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BossListElementComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(BossListElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
