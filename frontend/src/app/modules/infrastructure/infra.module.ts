import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CoreModule } from 'src/app/core/core.module';
import { UtilsModule } from 'src/app/utils/utils.module';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [UtilsModule, CoreModule, RouterModule],
  exports: [HomeComponent],
})
export class InfrastructureModule {}
